# Desenvolvimento de Aplicações Coorporativas

* **Curso:** [Análise e Desenvolvimento de Sistemas - IFPB Cajazeiras](http://www.ifpb.edu.br/campi/cajazeiras/cursos/cursos-superiores-de-tecnologia/analise-e-desenvolvimento-de-sistemas)
* **Professor:** Ricardo Job
* **Precisando de Ajuda?**
    * Consulte esta página ou crie uma  [issue](https://bitbucket.org/ricardojob/ads-dac/issues).
    * [Email do grupo](mailto:dac-20141@googlegroups.com) para comunicados entre professor e alunos.
	* [Email](mailto:ricardo.job@ifpb.edu.br) para dúvidas pontuais, ou para marcar uma reunião.
* **[Material da Disciplina](https://drive.google.com/folderview?id=0B3bNBHsD1S9gOUhNbzNfY2FRTkE) **
	* **[Notas da Disciplina](https://docs.google.com/spreadsheets/d/11bhwwcYrNhlQKkm05587bfAOL1RWwo7YIcf1ujvg41g/edit#gid=295168816)**
## Descrição da Disciplina


## Objetivo Geral

Permitir o aprendizado de conceitos e técnicas fundamentais necessários para integração e gerenciamento de aplicações corporativas.

## Objetivos Específicos

* Entender os fundamentos da Java Persistence API;
* Aplicar e gerenciar Session Beans;
* Aplicar e gerenciar Entity Beans;
* Aplicar e gerenciar Message DriveBeans;
* Compreender a Arquitetura Baseada em Componentes.

## Avaliação

* Projeto 1 (Primeira Parte) – 35%
* Projeto 2 (Segunda Parte) – 35%
* Atividade Contínuas – 30%

## Conteúdo Programático

### Semana 1
1. Introdução
	* Data: 15/05/2014.
	* Apresentação da disciplina.
	* Recursos Primários da plataforma Enterprise JavaBeans.
	* [Recursos Primários](https://docs.google.com/file/d/0B3bNBHsD1S9gRGg1TmZoTDFGMWc/)
	* Atividade Complementar.
2. Persistência Transparente
	* Data: 16/05/2014
	* Apresentação da atividade complementar
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gbVVxTXRlN3VUZTg/)
	* [Projeto exemplo - Atividade 1](https://bitbucket.org/ricardojob/ads-dac-atividade1/)
### Semana 2
3. Introdução a JPA
	* Data: 22/05/2014
	* Mapeamento Objeto Relacional
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gckVnNkJFU2QtN2M/)
4. Introdução a JPA + Exemplo
	* Data: 23/05/2014
	* Anotações básicas, exemplos e atividade prática.
	* Características do projeto: Maven, Git, JPA e BitBucket.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gU1dfMU42cWZTLUk/)
	* [Projeto exemplo - Atividade 2](https://bitbucket.org/ricardojob/ads-dac-atividade2/)
### Semana 3
5. Introdução a JPA + Exemplo
	* Data: 29/05/2014
	* Anotações básicas, exemplos e atividade prática.
	* [Continuação da Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gU1dfMU42cWZTLUk/)
	* Contexto de Persistência: Escopo de transação, Estendio e não sincronizado.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gLUg5ckNRclRUNEk/)
	* [Projeto exemplo - Atividade 3](https://bitbucket.org/ricardojob/ads-dac-atividade3/)
	* Atividade prática na próxima aula (05/06/2014).
	* Leitura do capítulo 6 do livro: Enterprise JavaBeans 3.0.
6. Atividade Prática 
	* Data: 30/05/2014
	* Anotações básicas, exemplos e atividade prática.
	* Construção de uma classe DAO para centralizar o acesso aos dados.
	* [Continuação da Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gLUg5ckNRclRUNEk/)
### Semana 4
7. Mapeamento de Atributos Simples
	* Data: 05/06/2014
	* Mapeamento de Atributos Simples, exemplos e atividade prática.
	* Atividade Prática: Acesso ao DAO construido na aula anterior e adição da anotação @ElementCollection.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gc3IyZms1dXlEdUE/)
	* [Projeto exemplo - Atividade 4](https://bitbucket.org/ricardojob/ads-dac-atividade4/)
8. Mapeamento de Relacionamento
	* Data: 06/06/2014
	* Mapeamento de Relacionamento entre Entidades
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gRzAxYlFBdlRReVk/)
	* [Projeto exemplo - Atividade 5](https://bitbucket.org/ricardojob/ads-dac-atividade5/)
	* [Atividade - Relacionamento de Entidades](https://docs.google.com/file/d/0B3bNBHsD1S9gR0hzVF9abmVyV3c/)
### Semana 5
9. Herança de Entidades
	* Data: 12/06/2014
	* Herança de Entidades, exemplos e atividade prática.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gOUZRdGRLb1pKMVE/)
	* [Projeto exemplo - Atividade 6](https://bitbucket.org/ricardojob/ads-dac-atividade6/)		
10. Projeto - Parte 1
	* Data: 13/06/2014
	* Apresentação do projeto.
	* [Projeto - Parte 1](https://docs.google.com/file/d/0B3bNBHsD1S9ga0l4T01zaVpObWc/)
	* [Projeto Arquitetural - Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gRlFJSFh3NlBtcUE/)
	* [Projeto Arquitetural - Exemplo](https://docs.google.com/file/d/0B3bNBHsD1S9gdU54N2hSRHUzNGc/)
	* [Consultas com JPA - Aula 1](https://docs.google.com/file/d/0B3bNBHsD1S9gajN1MTR4a0lsaE0/)
	* [Consultas com JPA - Aula 2](https://docs.google.com/file/d/0B3bNBHsD1S9gWmpkbU96ZnRST2M/)	
### Semana 6
11. Consulta - Aula 1
	* Data: 10/07/2014
	* Consulta de Entidades e exemplos.
	* Consultas simples, com parâmetros e paginação.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gWmpkbU96ZnRST2M/)
	* [Projeto exemplo - Atividade 7](https://bitbucket.org/ricardojob/ads-dac-atividade7/)		
12. Consulta - Aula 2
	* Data: 11/07/2014
	* Consulta de Entidades e exemplos.
	* Consulta: com construtor, usando IN e JOIN, usando LEFT JOIN, DISTINCT, com BETWEEN, IS NULL e IS EMTPY.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gWmpkbU96ZnRST2M/)
	* [Projeto exemplo - Atividade 7](https://bitbucket.org/ricardojob/ads-dac-atividade7/)
### Semana 7
13. Projeto - Parte 1
	* Data: 17/07/2014
	* Ordem de apresentação:
		1. oelisiany - Sistema de gerenciamento de Projetos de Pesquisa e extensão
		2. carloseduardollira - Escola
		3. denismarkdenis
		4. marcielmj - Livraria
		5. luciana.gadelha - Farmácia
		6. kelsonsd - Livraria 	
14. Projeto - Parte 2
	* Data: 18/07/2014
	* Ordem de apresentação:
		1. ads.izabel - Clínica Médica
		2. imedalu - 
		3. zefcruzbs - SisPDV sistema de ponto de venda(mercado)
		4. samuellbdias - Pet Shop
		5. joelanio - Academia de Musculação
		6. antoniofejunior - Pizzaria
		7. fernandodof - Loja de Calçados
### Semana 8
15. Revisão sobre o projeto
	* Data: 22/07/2014
	* (2 aulas)Aula extra para dúvidas sobre a execução do projeto	
16. Projeto - Parte 1
	* Data: 24/07/2014
	* Ordem de apresentação:
		1. denismarkdenis
		2. marcielmj - Livraria
		3. luciana.gadelha - Farmácia
		4. ads.izabel - Clínica Médica
		5. imedalu - 
		6. zefcruzbs - SisPDV sistema de ponto de venda(mercado)		
		7. joelanio - Academia de Musculação
		8. antoniofejunior - Pizzaria
17. Projeto - Parte 2
	* Data: 25/07/2014
	* Atividade 1 - Material até Introdução a JPA (Semana 2).	
	* [Projeto - Parte 2](https://docs.google.com/file/d/0B3bNBHsD1S9gdlZVSWZFZnNPRWs/)
### Semana 9
18. Revisão sobre o projeto
	* Data: 29/07/2014
	* (2 aulas) Apresentação dos projetos
19. Aula Cancelada
	* Data: 31/07/2014
	* Não houve aula. Motivo: Professor doente
20. Atividade 2
	* Data: 01/08/2014
	* Atividade 2 - Contexto de Persistência e Mapeamento Simples.	
### Semana 10
21. Consulta 
	* Data: 07/08/2014
	* Consulta de Entidades e exemplos.
	* Consulta: MEMBER OF, LIKE, Expressões Funcionais, Funções Agregadas, Subconsultas e Query Nomeadas.
	* [Aula](https://docs.google.com/file/d/0B3bNBHsD1S9gWmpkbU96ZnRST2M/)
	* [Projeto exemplo - Atividade 7](https://bitbucket.org/ricardojob/ads-dac-atividade7/)
22. Atividade 3
	* Data: 08/08/2014
	* Atividade 3 - Relacionamentos de Entidade e Herança.	
### Semana 11
23. Session Bean
	* Data: 14/08/2014
	* Stateless: Definição e prática
	* [Aula](https://drive.google.com/file/d/0B3bNBHsD1S9gYXpOc1kyUmRxLVk/)
	* [Projeto exemplo - Atividade 8](https://bitbucket.org/ricardojob/ads-dac-atividade8/)
24. Atividade 4
	* Data: 15/08/2014
	* Atividade 4 - Consultas.	
### Semana 12
25. Session Bean 
	* Data: 18/08/2014
	* (4 aulas) Aula extra sobre Stateless: Definição e prática.
	* [Aula](https://drive.google.com/file/d/0B3bNBHsD1S9gYXpOc1kyUmRxLVk/)
	* [Projeto exemplo - Atividade 9](https://bitbucket.org/ricardojob/ads-dac-atividade9/)
26. Stateful
	* Data: 21/08/2014
	* Stateful: Definição e exemplos.
	* [Aula](https://drive.google.com/file/d/0B3bNBHsD1S9gYXpOc1kyUmRxLVk/)
	* [Projeto exemplo - Atividade 10](https://bitbucket.org/ricardojob/ads-dac-atividade10/)
	* **[Projeto exemplo - Arquitetura do Projeto](https://bitbucket.org/ricardojob/ads-dac-projetoexemplo)**
### Semana 13
27. Session Bean 
	* Data: 25/08/2014
	* (2 Aulas) Singleton: Definição e Exemplos.
	* [Aula](https://drive.google.com/file/d/0B3bNBHsD1S9gYXpOc1kyUmRxLVk/)
27. MessageDriven Beans
	* Data: 28/08/2014
	* Singleton: Definição e Exemplos.
	* [Aula]()
28. MessageDriven Beans
	* Data: 29/08/2014
	* MessageDriven Beans: Definição e exemplos.
	* [Aula]()
	
			
## Bibliografia
* Burke, B. Enterprise Javabeans 3.0. Pearson, 2007.    
* Schincariol, M.  EJB3 Profissional: Java Persistence API. Ciência Moderna, 2008.  
* Bauer C.; King G. Java Persistance com Hibernate.Ciência Moderna, 1ª edição, 2007. 
* Derek, L. EJB3 Em Ação. Alta Books, 2ª Edição, 2009.
* Goncalves, A. Introdução à Plataforma Java EE 6 com GlasssFish 3. Ciência Moderna, 2ª edição, 2011. 

## Referências Online
* [Igor. Novidades no EJB 3.1](http://www.matera.com/br/2013/01/novidades-no-ejb-3-1/)
* [Josh Long. Java EE6: EJB3.1 é uma evolução irresistível](http://www.infoq.com/br/news/2010/02/jee6_ejb_31/)
* [K19. Apostilas](http://www.k19.com.br/downloads/apostilas/)

